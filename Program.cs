﻿using System;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            //Dom1
            AClass dom1 = new HBuilder1();
            dom1.TemplateMethod();

            //Dom2
            AClass dom2 = new HBuilder2();
            dom2.TemplateMethod();


        }

        abstract class AClass
        {
            public abstract void Roof();
            public abstract void Walls();

            public void TemplateMethod()
            {
                Roof();
                Walls();
                Console.WriteLine("");
            }
        }

        class HBuilder1 : AClass
        {
            public override void Roof()
            {
                Console.WriteLine("  ____||____\n ///////////\\\n///////////  \\ ");
            }
            public override void Walls()
            {
                Console.WriteLine("|    _    |  |\n|[] | | []|[]|\n|   | |   |  |");
            }
        }
        class HBuilder2 : AClass
        {
            public override void Roof()
            {
                Console.WriteLine("  ______||__ \n ///\\////\\//\\\n///////////  \\");
            }
            
            public override void Walls()
            {
                Console.WriteLine("| _   __  |  |\n|| | [__] |[]|\n|| |      |  |");
            }
        }

    }
}
